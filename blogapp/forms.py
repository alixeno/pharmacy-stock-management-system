from django import forms
from .models import *
from django.contrib.auth.models import User

# from django_summernote.widgets import SummernoteWidget


class LoginForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Username...',
		'id': 'formfield',
		}))
	password = forms.CharField(widget = forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'Password...',
		'id': 'formfield',
		}))


class SignupForm(forms.ModelForm):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Username',
		'id': 'formfield',
		}))
	email = forms.EmailField(widget = forms.EmailInput(attrs={
		'class': 'form-control',
		'placeholder': 'Email',
		'id': 'formfield',
		}))
	password = forms.CharField(widget = forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'password',
		'id': 'formfield',
		}))
	confirm_password = forms.CharField(widget = forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'password again',
		'id': 'formfield',
		}))
	class Meta:
		model = Customer
		fields = ['username', 'email', 'password', 'confirm_password', 'name', 'mobile', 'country', 'gender']

	def clean_username(self):
		username = self.cleaned_data.get("username")
		user = User.objects.filter(username = username)

		if user.exists():
			raise forms.ValidationError(
				"...!!!User with this Username Already Exists...!!!"
				)
		return username

	def clean_email(self):
		email = self.cleaned_data.get("email")
		user = User.objects.filter(email = email)

		if user.exists():
			raise forms.ValidationError(
				" ---!!! Email Already Taken !!!--- "
				)
		return email

	def clean_confirm_password(self):
		pwrd = self.cleaned_data.get("password")
		c_password = self.cleaned_data.get("confirm_password")
		if pwrd != c_password:
			raise forms.ValidationError(
				"---!!! Passwords didn't match !!!---"
				)
		return c_password


class BlogForm(forms.ModelForm):
	class Meta:
		model = Blog
		exclude = ['blog_views']
		fields = ['title', 'image', 'description', 'author']
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control',
				'placeholder': 'Enter Blog Title',
				'id': 'formfield',
				}),
			'image': forms.ClearableFileInput(attrs={
				'class': 'form-control',
				'id': 'formfield',
				}),
			'description': forms.Textarea(attrs={
				'class': 'form-control',
				'placeholder': 'Enter Description About Blog...',
				'id': 'formfield',
				}),
			# 'description': SummernoteWidget(),
			'author': forms.TextInput(attrs={
				'class': 'form-control',
				'placeholder': 'Enter author name...',
				'id': 'formfield',
				})
		}

