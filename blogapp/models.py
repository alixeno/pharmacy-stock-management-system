from django.db import models
from django.contrib.auth.models import User, Group


class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)
	active = models.BooleanField(default = True)

	class Meta:
		abstract = True


GENDER_CHOICES = (
	('male', 'Male'),
	('female', 'Female'),
	('other', 'Other'),
	)


class Customer(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	name = models.CharField(max_length = 300)
	email = models.EmailField()
	mobile = models.CharField(max_length = 30, null = True, blank = True)
	country = models.CharField(max_length = 30)
	gender = models.CharField(max_length = 10, choices = GENDER_CHOICES)

	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name = "customer")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name


class Blog(TimeStamp):
	title = models.CharField(max_length = 300)
	image = models.ImageField(upload_to = 'blog')
	description = models.TextField()
	author = models.CharField(max_length = 300)
	blog_views = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.title + "(By: " + self.author + ")"


class Comment(TimeStamp):
	blog = models.ForeignKey(Blog, on_delete = models.CASCADE)
	commenter = models.CharField(max_length = 300)
	email = models.EmailField()
	comment = models.TextField()

	def __str__(self):
		return "Commented by: " + self.commenter


