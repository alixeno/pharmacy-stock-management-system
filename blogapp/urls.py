from django.urls import path
from .views import *

app_name = "blogapp"


urlpatterns = [
	
	path("login/", LoginView.as_view(), name = "login"),
	path("logout/", LogoutView.as_view(), name = "logout"),
	path("signup/", SignupView.as_view(), name = "signup"),

	path("", BlogHomeView.as_view(), name = "bloghome"),
	path("list/", BlogListView.as_view(), name = "bloglist"),
	path("detail/<int:pk>/", BlogDetailView.as_view(), name = "blogdetail"),
	path("create/", BlogCreateView.as_view(), name = "blogcreate"),
	path("update/<int:pk>/", BlogUpdateView.as_view(), name = "blogupdate"),
	path("delete/<int:pk>/", BlogDeleteView.as_view(), name = "blogdelete"),
	path("manage/", BlogManageView.as_view(), name = "blogmanage"),
		
]