from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy

from django.http import JsonResponse



class LoginView(FormView):
	template_name = "signintemplates/login.html"
	form_class = LoginForm
	success_url = reverse_lazy("blogapp:bloghome")

	def form_valid(self, form):
		uname = form.cleaned_data['username']
		pwrd = form.cleaned_data['password']
		user = authenticate(username = uname, password = pwrd)

		if User is not None:
			login(self.request, user)
		else:
			return render(self.request, self.template_name,{
			'error': 'Invalid Username or Password!!!',
			'form': form
			})

		# if user is not None:
		# 	login(self.request, user)
		# 	data = {
		# 		'success': "Logged In Successfully"
		# 	}
		# 	return JsonResponse(data)
		# else:
		# 	data = {
		# 	'error': 'Invalid Username Or Password',
		# 	}
		# 	return JsonResponse(data)	
		return super().form_valid(form)


class LogoutView(View):
	def get(self, request):
		logout(request)
		return redirect('/blog/')


class SignupView(CreateView):
	template_name = "signintemplates/signup.html"
	form_class = SignupForm
	success_url = reverse_lazy("blogapp:bloghome")

	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		email = form.cleaned_data["email"]
		password = form.cleaned_data["password"]
		user = User.objects.create_user(uname, email, password)
		form.instance.user = user

		return super().form_valid(form)



class BlogHomeView(TemplateView):
	template_name = "blogtemplates/bloghome.html"
	

class BlogListView(ListView):
	template_name = "blogtemplates/bloglist.html"
	queryset = Blog.objects.all().order_by("-id")
	context_object_name = "allblogs"
	paginate_by = 6

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['mostwatchblogs'] = Blog.objects.all().order_by("-blog_views")

		return context


class BlogDetailView(TemplateView):
	template_name = "blogtemplates/blogdetail.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		blog_id = self.kwargs["pk"]
		blog_object = Blog.objects.get(id = blog_id)
		context["blogobject"] = blog_object
		print(blog_object.blog_views)
		# context["blogobject"] = blog_object
		blog_object.blog_views = blog_object.blog_views+1
		blog_object.save()
		print(context)

		return context


class BlogCreateView(CreateView):
	template_name = "blogtemplates/blogcreate.html"
	form_class = BlogForm
	success_url = reverse_lazy('blogapp:bloglist')


class BlogUpdateView(UpdateView):
	template_name = "blogtemplates/blogupdate.html"
	form_class = BlogForm
	model = Blog
	success_url = reverse_lazy("blogapp:bloglist")


class BlogDeleteView(DeleteView):
	template_name = "blogtemplates/blogdelete.html"
	model = Blog
	success_url = reverse_lazy("blogapp:bloglist")

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		blog_id = self.kwargs["pk"]
		blog = Blog.objects.get(id = blog_id)

		return context

class BlogManageView(TemplateView):
	template_name = "blogtemplates/blogmanage.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		blog_obj = Blog.objects.all().order_by("-id")

		page = self.request.GET.get('page',1)

		paginator = Paginator(blog_obj, 10)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allblogs"] = results

		return context