from django.urls import path
from .views import *

app_name = "diaryapp"


urlpatterns = [
	path("", DiaryHomeView.as_view(), name = "diaryhome"),
]