from django.contrib import admin
from .models import *

admin.site.register([Pharmacy, Manufacturer, Supplier, Batch, MedicineCategory, Medicine, SingleMedicine, Purchase, Sale])