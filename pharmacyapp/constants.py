


PAYMENT_STATUS = (
	("fullpayment", "Full Payment"),
	("partialpayment", "Partial Payment"),
	("fullcredit", "Full Credit"),
	)


PAYMENT_METHOD = (
	("cashpayment", "Cash Payment"),
	("chequepayment", "Cheque Payment"),
	("epayment", "E-Payment"),
	)