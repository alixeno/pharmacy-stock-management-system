from django.db import models
from django.contrib.auth.models import User, Group
from .constants import *


class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)
	active = models.BooleanField(default = True)

	class Meta:
		abstract = True


class Pharmacy(TimeStamp):
	name = models.CharField(max_length = 300)
	profile_image = models.ImageField(upload_to = 'pharmacy')
	address = models.CharField(max_length = 300)
	estd_date = models.DateField()
	slogan = models.CharField(max_length = 300, null = True, blank = True)
	owner = models.CharField(max_length = 300)
	contact_no = models.CharField(max_length = 30)
	email = models.EmailField()
	map_location = models.CharField(max_length = 300)
	facebook_id = models.CharField(max_length = 300, null = True, blank = True)
	website = models.CharField(max_length = 300, null = True, blank = True)
	logo = models.ImageField(upload_to = 'pharmacy')

	def __str__(self):
		return self.name + " (" + self.owner + ")"


class Admin(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	name = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = 'user')
	mobile = models.CharField(max_length = 30)
	email = models.EmailField(null = True, blank = True)
	address = models.CharField(max_length = 300)

	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name = "admin")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name


class Manufacturer(TimeStamp):
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300, null = True, blank = True)
	contact_no = models.CharField(max_length = 30)
	profile_image = models.ImageField(upload_to = 'manufacturer', null = True, blank = True)

	def __str__(self):
		return self.name


class Supplier(TimeStamp):
	name = models.CharField(max_length = 300)
	contact_no = models.CharField(max_length = 30)
	manufacturer = models.ForeignKey(Manufacturer, on_delete = models.CASCADE)

	def __str__(self):
		return self.name


class Batch(TimeStamp):
	name = models.CharField(max_length = 300)
	manufacturer = models.ForeignKey(Manufacturer, on_delete = models.CASCADE)
	supplier = models.ManyToManyField(Supplier)
	manufacture_date = models.DateField()
	expire_date = models.DateField()

	def __str__(self):
		return self.name


class MedicineCategory(TimeStamp):
	title = models.CharField(max_length = 300)
	sku = models.CharField(max_length = 300, unique = True, null = True, blank = True)
	root = models.ForeignKey('self', on_delete = models.SET_NULL, null = True, blank = True)
	description = models.TextField()

	def __str__(self):
		return self.title


class Medicine(TimeStamp):
	title = models.CharField(max_length = 300)
	medicine_category = models.ForeignKey(MedicineCategory, on_delete = models.CASCADE)
	sku = models.CharField(max_length = 300, unique = True, null = True, blank = True)
	rack = models.CharField(max_length = 300, null = True, blank = True)

	def __str__(self):
		return self.title


# class MedicineStatus(TimeStamp):
# 	title = models.CharField(max_length = 20)

# 	def __str__(self):
# 		return self.title


class SingleMedicine(TimeStamp):
	title = models.CharField(max_length = 300, null = True, blank = True)
	medicine = models.ForeignKey(Medicine, on_delete = models.CASCADE)
	batch = models.ForeignKey(Batch, on_delete = models.CASCADE, null = True, blank = True)
	medicine_category = models.ForeignKey(MedicineCategory, on_delete = models.CASCADE, null = True, blank = True)
	manufacturer = models.ForeignKey(Manufacturer, on_delete = models.SET_NULL, null = True, blank = True)
	supplier = models.ForeignKey(Supplier, on_delete = models.SET_NULL, null = True, blank = True)
	cost_price = models.DecimalField(max_digits = 20, decimal_places = 2, null = True, blank = True)
	selling_price = models.DecimalField(max_digits = 20, decimal_places = 2, null = True, blank = True)
	quantity = models.PositiveIntegerField()
	subtotal = models.DecimalField(max_digits = 20, decimal_places = 2)
	# exp_status = models.ForeignKey(MedicineStatus, on_delete = models.SET_NULL, null = True, blank = True)

	def __str__(self):
		return self.title


class Purchase(TimeStamp):
	manufacturer = models.OneToOneField(Manufacturer, on_delete = models.CASCADE)
	supplier = models.OneToOneField(Supplier, on_delete = models.CASCADE)
	invoice_no = models.CharField(max_length = 300, null = True, blank = True)
	purchase_date = models.DateField()
	medicine = models.ForeignKey(SingleMedicine, on_delete = models.CASCADE)
	subtotal = models.DecimalField(max_digits = 30, decimal_places = 2)
	discount = models.DecimalField(max_digits = 30, decimal_places = 2, null = True, blank = True)
	total = models.DecimalField(max_digits = 30, decimal_places = 2)
	payment_status = models.CharField(max_length = 300, choices = PAYMENT_STATUS)
	payment_method = models.CharField(max_length = 300, choices = PAYMENT_METHOD, null = True, blank = True)

	def __str__(self):
		return "From " + str(self.manufacturer) + " of total: Rs." + str(self.total)


class Sale(TimeStamp):
	customer = models.CharField(max_length = 300)
	age = models.PositiveIntegerField(null = True, blank = True)
	description = models.TextField(null = True, blank = True)
	sales_date = models.DateField()
	medicine = models.ForeignKey(SingleMedicine, on_delete = models.CASCADE)
	subtotal = models.DecimalField(max_digits = 30, decimal_places = 2)
	discount = models.DecimalField(max_digits = 30, decimal_places = 2, null = True, blank = True)
	total = models.DecimalField(max_digits = 30, decimal_places = 2)
	payment_status = models.CharField(max_length = 300, choices = PAYMENT_STATUS)
	payment_method = models.CharField(max_length = 300, choices = PAYMENT_METHOD, null = True, blank = True)

	def __str__(self):
		return str(self.customer) + "(Rs." + str(self.total) + ")"


