from django.urls import path
from .views import *

app_name = "pharmacyapp"

urlpatterns = [

# Admin URLS
	path("pharmacy-admin/", AdminHomeView.as_view(), name = "adminhome" ),
	path("pharmacy-admin/medicine/list/", AdminMedicineListView.as_view(), 
		name = "adminmedicinelist"),
	path("pharmacy-admin/medicine/<int:pk>/detail/", AdminMedicineDetailView.as_view(), 
		name = "adminmedicinedetail"),
	path("pharmacy-admin/single-medicine/<int:pk>/detail/", AdminSingleMedicineDetailView.as_view(), 
		name = "adminsinglemedicinedetail"),

	path("pharmacy-admin/batch/list/", AdminBatchListView.as_view(), 
		name = "adminbatchlist"),

	path("pharmacy-admin/company/list/", AdminCompanyListView.as_view(), 
		name = "admincompanylist"),
	path("pharmacy-admin/supplier/list/", AdminSupplierListView.as_view(), 
		name = "adminsupplierlist"),




# Client URLS

	path("", HomeView.as_view(), name = 'home' ),



]