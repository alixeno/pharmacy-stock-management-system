from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from django.utils import timezone
from datetime import datetime

now = timezone.now()




# Admin ViewClass

class AdminMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["allpharmacy"] = Pharmacy.objects.all()
		context["allsinglemedicines"] = SingleMedicine.objects.all().order_by("-exp_status_id")
		return context


class AdminHomeView(AdminMixin, TemplateView):
	template_name = "admintemplates/adminhome.html"


class AdminMedicineListView(AdminMixin, TemplateView):
	template_name = "admintemplates/adminmedicinelist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		medicine_obj = Medicine.objects.all().order_by("title")

		page = self.request.GET.get('page',1)

		paginator = Paginator(medicine_obj, 15)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["expired_medicine"] = SingleMedicine.objects.filter(batch__expire_date__lte = now)
		context["allmedicines"] = results

		return context 


# class AdminMedicineDetailView(AdminMixin, DetailView):
# 	template_name = "admintemplates/adminmedicinedetail.html"
# 	model = Medicine
# 	context_object_name = "medicineobject"

# 	def get_context_data(self, **kwargs):
# 		context = super().get_context_data(**kwargs)
# 		context["now"] = datetime.now()
# 		return context


class AdminMedicineDetailView(AdminMixin, TemplateView):
	template_name = "admintemplates/adminmedicinedetail.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		medicine_id = self.kwargs["pk"]
		medicine_obj = Medicine.objects.get(id = medicine_id)
		context["medicineobject"] = medicine_obj
		single_obj = SingleMedicine.objects.filter(medicine_id = medicine_id)
		context["freshmedicine"] = single_obj.exclude(batch__expire_date__lte = now).order_by("batch__expire_date")
		context["expiredmedicine"] = single_obj.filter(batch__expire_date__lte = now)
		context["now"] = datetime.now()

		return context


class AdminSingleMedicineDetailView(AdminMixin, TemplateView):
	template_name = "admintemplates/adminsinglemedicinedetail.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		singlemedicine_id = self.kwargs["pk"]
		single_obj = SingleMedicine.objects.get(id = singlemedicine_id)
		context["singlemedicineobject"] = single_obj
		context["now"] = datetime.now()

		return context


class AdminBatchListView(AdminMixin, ListView):
	template_name = "admintemplates/adminbatchlist.html"
	queryset = Batch.objects.all().order_by("-id")
	context_object_name = "allbatch"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["now"] = datetime.now()

		return context


class AdminCompanyListView(AdminMixin, ListView):
	template_name = "admintemplates/admincompanylist.html"
	queryset = Manufacturer.objects.all().order_by("-id")
	context_object_name = "allcompany"
	paginate_by = 10


class AdminSupplierListView(AdminMixin, ListView):
	template_name = "admintemplates/adminsupplierlist.html"
	queryset = Supplier.objects.all().order_by("-id")
	context_object_name = "allsupplier"
	paginate_by = 10









# Client ViewClass



class HomeView(TemplateView):
	template_name = 'home.html'